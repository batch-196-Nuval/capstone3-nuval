import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Page Not Found",
        content: "Sorry, the Page you are looking for does not exist or is not available...",
        destination: "/balamb-garden",
        label: "Go Back Home"
    }
    
    return (
        <Banner data={data}/>
    )
}
