import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	const data = {
        title: "Squall's Treasure Trove",
        content: "Only the Finest Weapons for your Final Fantasy VIII Weapon Needs!",
        destination: "/products/active",
        label: "Check it out Now"
    }

    return (
        <>
	        <Banner data={data}/>
	        <Highlights />
		</>

    )
}