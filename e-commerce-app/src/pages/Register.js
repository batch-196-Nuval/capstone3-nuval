import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);



	function registerUser(e){
		 // prevents page redirection via form submission
		 e.preventDefault();

		 fetch('http://localhost:4000/users/verify-email', {
		 	method: 'POST',
		 	headers: {
		 		'Content-Type' : 'application/json'
		 	},
		 	body: JSON.stringify({
		 		email:email
		 	})
		 })
		 .then(res => res.json())
		 .then(data => {

		 	if(data){
		 		Swal.fire({
		 			title: "Duplicate Email Address found!",
		 			icon: "info",
		 			text: "The email that you're trying to register already exists in the system. Kindly use a different Email Adress then."
		 		});

		 	} else {
		 		
		 		fetch('http://localhost:4000/users/register', {
		 			method: 'POST',
		 			headers: {
		 				'Content-Type' : 'application/json'
		 			},
		 			body: JSON.stringify({
		 				firstName: firstName,
		 				lastName: lastName,
		 				email: email,
		 				mobileNo: mobileNo,
		 				password: password
		 			})
		 		})
		 		.then(res => res.json())
		 		.then(data => {
		 			console.log(data);

		 			if(data.email){
		 				Swal.fire({
		 					title: 'Registration Successful!',
		 					icon: 'success',
		 					text: 'Thank you for registering. We hope that you will enjoy being part of our community!'
		 				});
		 				
		 				history("/login");

		 			} else {
		 				Swal.fire({
		 					title: 'Registration Failed!',
		 					icon: 'error',
		 					text: 'Something went wrong! Please try again.'
		 				});
		 			};
		 		});
		 	};
		 });

		 // clear input fields
		 setEmail('');
		 setPassword('');
		 setFirstName('');
		 setLastName('');
		 setMobileNo('');


	};



	useEffect(() => {

		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, mobileNo, password]);




	return(
		(user.id !== null)?
			<Navigate to="/products/active"/>
		:
		<>
		<h1>Registration</h1>
		<p>To be able to access different parts of our website, kindly register. Thank you!</p>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your First Name here..."
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your Last Name here..."
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="number"
					placeholder="Please enter your 11-digit Mobile Number here..."
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please enter your Email Address here..."
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We will never share your Email Address with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please enter your Password here..."
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive ?			
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}

		</Form>
		</>

	)
}
