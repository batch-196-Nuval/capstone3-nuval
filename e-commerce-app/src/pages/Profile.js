import {useState, useEffect, useContext} from 'react';
import {Row} from 'react-bootstrap'
import Prof from '../components/Prof';
import UserContext from '../UserContext';

export default function Profile(){

	const {user} = useContext(UserContext);
	

		const [details, setDetails] = useState([]);

		useEffect(() => {
		fetch('http://localhost:4000/users/details')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const detailsArr = (data.map(detail => {

				return (
					<Prof key={user._id} profileProp={detail} breakpoint={4} />
				)
				
			}))
			setDetails(detailsArr)
		})

	}, [details])





		






	
	return (

		<>
			<h1>Profile</h1>
			<p>For now, you can only view your details here. In the future, we will update the functionality of this page.</p>
				<Row className="mt-3 mb-3">
					{details}
				</Row>
		</>
	)
}

// process of passing through the props
// from the Courses Page -> CourseCard = PHP Laravel
// from CourseCard -> props === parameter

// courseProp from Courses Page ===  props parameter from CourseCard