import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap'
import Prods from '../components/Prods';

export default function AvailableProducts(){
	
	const [products, setProducts] = useState([]);

		useEffect(() => {
		fetch('http://localhost:4000/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const productsArr = (data.map(product => {

				return (
					<Prods key={product._id} productProp={product} breakpoint={4} />
				)
				
			}))
			setProducts(productsArr)
		})

	}, [products])


	
	return(

		<>
			<h1>All Products</h1>
			<p>Here is the list of All Products regardless if Active / Available or Inactive / Not Available.</p>
				<Row className="mt-3 mb-3">
						{products}
				</Row>
		</>
	)
}

// process of passing through the props
// from the Courses Page -> CourseCard = PHP Laravel
// from CourseCard -> props === parameter

// courseProp from Courses Page ===  props parameter from CourseCard