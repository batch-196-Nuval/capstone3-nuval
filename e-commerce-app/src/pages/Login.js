import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

    const {user, setUser} = useContext(UserContext);
    console.log(user);


	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(false);

      	function authenticate(e) {

	        // Prevents page redirection via form submission
	        e.preventDefault();


            fetch('http://localhost:4000/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(typeof data.accessToken !== "undefined"){
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful!",
                        icon: "success",
                        text: "Welcome back to Squall's Treasure Trove!"
                    });

                } else {

                    Swal.fire({
                        title: "Authentication Failed!",
                        icon: "error",
                        text: "Kindly check your credentials..."
                    });

                };
            });

            
	        setEmail('');
	        setPassword('');



	    };

        const retrieveUserDetails = (token) => {

            console.log(token);
            fetch('http://localhost:4000/users/details', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log("What is the data passed?")
                console.log(data)
                console.log("The data passed is aboved!")

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })
        }; 

    useEffect(() => {
	    // Validation to enable submit button when all fields are populated and both passwords match
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }

	}, [email, password]);


    
    return (

        (user.id !== null) ?

            <Navigate to="/users/profile"/>

            :

            <>
                <h1>Login</h1>
                <p>Login to your Account here!</p>
                <Form onSubmit={e => authenticate(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address:</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter your Email Address here..." 
                            required
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}

                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control 
                         type="password" 
                         placeholder="Enter your Password here..." 
                         required
                         value={password}
                         onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>

                    <p className="text-muted">Not yet registered? Be part of our growing Community! Please register <Link to="/register">here</Link>.</p>

                    { isActive ? 
                        <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                            Login
                        </Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn" disabled className="mt-3 mb-5">
                         Login
                        </Button>
                    }

                </Form>
            </>

        )
}
