import {Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Prods({productProp}){
	
	const {name, description, price, _id} = productProp;

	return(

		<Col xs={12} md={6} lg={4}>
			<Card className="mb-3">
				<Card.Body>
					<Card.Title className="fw-bold">{name}</Card.Title>
					<Card.Subtitle> Product Description: </Card.Subtitle>
					<Card.Text>
						{description}
					</Card.Text>
					<Card.Subtitle> Product Price: </Card.Subtitle>
					<Card.Text>{price} Gil</Card.Text>
					<Link className="btn btn-primary" to={`/single-product/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>
		</Col>

	)
}
