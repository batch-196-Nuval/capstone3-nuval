import {useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';
import Logo from '../images/squall-logo.jpg';


export default function AppNavbar(){

	const {user} = useContext(UserContext);

	// state hook to store the user information stored in the login page (without provider)
	// const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user);

	return (
	
	<Navbar bg="light" expand="lg">
	  <Container>
	    <Navbar.Brand as={Link} to="/balamb-garden">
	    	<img alt=""
			src={Logo}
			height="50"
			width="50"
			className="img-fluid rounded-circle d-inline-block align-center"/>{" "}Squall's Treasure Trove &copy;
	    </Navbar.Brand>
	     <Navbar.Toggle aria-controls="basic-navbar-nav" />
	       <Navbar.Collapse id="basic-navbar-nav">
	        <Nav className="me-auto">         
			   <Nav.Link as={Link} to="/balamb-garden">Home</Nav.Link>
			  {
			  	(user.isAdmin === true) ?
	           <Nav.Link as={Link} to="/products/all">Products</Nav.Link>
	           :
	           <Nav.Link as={Link} to="/products/active">Products</Nav.Link>
				}

			{
				(user.id !== null) ?
				<>
					<Nav.Link as={Link} to="/uers/profile">Profile</Nav.Link>
					<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
				</>	
					:
				<>
					<Nav.Link as={Link} to="/login">Login</Nav.Link>
	           		<Nav.Link as={Link} to="/register">Register</Nav.Link>
	           	</>
	     	}

	         </Nav>
	       </Navbar.Collapse>
	   </Container>
	</Navbar>

	)
};
