import {Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Prof(props){
	
	const {breakpoint, profileProp} = props;

	const {firstName, lastName, email, mobileNo, _id} = profileProp;

	return(

		<Col xs={12} md={6} lg={4}>
			<Card className="mb-3">
				<Card.Body>
					<Card.Title className="fw-bold">{firstName} {lastName}</Card.Title>
					<Card.Subtitle> Email Address: </Card.Subtitle>
					<Card.Text>
						{email}
					</Card.Text>
					<Card.Subtitle> Mobile Number: </Card.Subtitle>
					<Card.Text>{mobileNo}</Card.Text>
				</Card.Body>
			</Card>
		</Col>

	)
}