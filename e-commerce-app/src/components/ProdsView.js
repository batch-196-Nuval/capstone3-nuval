import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProdsView(){

	const {user} = useContext(UserContext);

	// allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const history = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	// useParams hook allows use to retrieve the courseId passed via the URL
	const {productId} = useParams();

	const order = (productId) => {

		fetch('http://localhost:4000/orders/', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: 'Ordered Successfully!',
					icon: 'success',
					text: 'Thank you for the patronage!'
				});

				history("/products");

			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again later!'
				});
			};

		});
	};

	useEffect(() => {
		console.log(productId)
		fetch(`http://localhost:4000/products/single-product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});

	}, [productId]);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card className="text-center">
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price} Gil</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" onClick={() => order(productId)}>Order</Button>
								:
								<Link className="btn btn-danger" to="/login">Login to Order</Link>
							}
						</Card.Body>
							<Link className="btn btn-primary" to="/products/active">Go Back</Link>
					</Card>

				</Col>
			</Row>
		</Container>

	)
}