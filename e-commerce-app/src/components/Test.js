import {Col, Card} from 'react-bootstrap';
import {Link, Navigate} from 'react-router-dom';


export default function Test({testProp}){
	
	const {firstName, lastName, email, mobileNo, _id} = testProp;

	if (localStorage["token"]) {

		return (

			<Col xs={12} md={6} lg={4}>
				<Card className="mb-3">
					<Card.Body>
						<Card.Title className="fw-bold">{firstName}</Card.Title>
						<Card.Subtitle> Email Address: </Card.Subtitle>
						<Card.Text>
							{email}
						</Card.Text>
						<Card.Subtitle> Mobile Number: </Card.Subtitle>
						<Card.Text>{mobileNo}</Card.Text>
						<Link className="btn btn-primary" to={`/balamb-garden`}>Go Back Home</Link>
					</Card.Body>
				</Card>
			</Col>

		

		)

	} else {

		return (

			<Navigate to="*"/>

		)

	}

	
}

